package NodeComplimentors;

import javafx.animation.Animation;
import javafx.animation.Animation.Status;
import javafx.animation.ScaleTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.effect.Shadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class ResponsiveButton extends Button {

	private Background background;
	private Shadow shadow;
	private double fromx,fromy,fromz;
	//private Rectangle backgroungRect;
	private boolean bResponsive = true;
	interface Response{
		void respondHoverInn();
		void respondHoverOut();
	}
	private Response response;
	private byte b = 0;
	public boolean noDefaultFill = false;
	/*
	 * my scalling transition on the button that will trigger when focused
	 */
	private ScaleTransition scaleT;

	public ResponsiveButton() {
		super();
		constructorWork();
	}

	public ResponsiveButton(String arg0, Node arg1) {
		super(arg0, arg1);
		constructorWork();
	}

	public ResponsiveButton(String arg0) {
		super(arg0);
		constructorWork();
	}

	/**
	 * Button's constructor's implementation.
	 */

	private void constructorWork(){
		scaleT = new ScaleTransition(Duration.millis(500), this);
		scaleT.statusProperty().addListener(new ChangeListener<Animation.Status>() {

			@Override
			public void changed(ObservableValue<? extends Status> arg0,
					Status arg1, Status arg2) {
				if(arg2 != null && arg2 ==Status.STOPPED){
					if(b ==0){
						if(response != null)
							response.respondHoverInn();
					}else if(b == 1){
						if(response != null)
							response.respondHoverOut();
					}
				}

			}
		});
		//scaleT.setAutoReverse(true);
		setEffect(shadow);
		fromx = getScaleX();
		fromy = getScaleY();
		fromz = getScaleZ();
		backgroundslides(false);
		setTextFill(Color.WHITESMOKE);
		setOnMouseEntered((EventHandler<? super MouseEvent>) sensorsDectors(SensorIdentifier.mHovered_In));
		setOnMouseExited((EventHandler<? super MouseEvent>) sensorsDectors(SensorIdentifier.mHovered_Out));
		setOnTouchPressed((EventHandler<? super TouchEvent>) sensorsDectors(SensorIdentifier.tHovered_In));
		setOnTouchReleased((EventHandler<? super TouchEvent>) sensorsDectors(SensorIdentifier.mHovered_Out));
		setContentDisplay(ContentDisplay.CENTER);
	}

	private enum SensorIdentifier{
		tHovered_In, tHovered_Out,mHovered_In, mHovered_Out;
	}

	private EventHandler<?> sensorsDectors(SensorIdentifier si){
		switch (si) {

		case tHovered_Out:

			return new EventHandler<TouchEvent>() {

				@Override
				public void handle(TouchEvent event) {
					if(bResponsive)
						if(fromx < getScaleX() && fromy < getScaleY()){
							scaleT.setByX(-0.2f);
							scaleT.setByY(-0.2f);
							scaleT.play();
							scaleChecker();
							b =1;
						}
					if(!noDefaultFill)
						backgroundslides(false);
				}
			};

		case tHovered_In:

			return new EventHandler<TouchEvent>() {

				@Override
				public void handle(TouchEvent event) {
					if(bResponsive)
						if(fromx == getScaleX() && fromy == getScaleY()){
							scaleT.setByX(0.2f);
							scaleT.setByY(0.2f);
							scaleT.play();
							b =0;
						}
				}
			};

		case mHovered_In:

			return new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {
					if(bResponsive)
						if(fromx == getScaleX() && fromy == getScaleY()){
							scaleT.setByX(0.2f);
							scaleT.setByY(0.2f);
							scaleT.play();
							b =0;
						}
				}
			};
		case mHovered_Out:
			return new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {
					if(bResponsive)
						if(fromx < getScaleX() && fromy < getScaleY()){
							scaleT.setByX(-0.2f);
							scaleT.setByY(-0.2f);
							scaleT.play();
							scaleChecker();
							b =1;
						}
					if(!noDefaultFill)
						backgroundslides(false);
				}
			};

		default:
			return new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {

				}
			};
		}


	}

	@Override
	public void fire() {		
		super.fire();
		if (!noDefaultFill)
			backgroundslides(true);
	}

	private void backgroundslides(boolean fire){
		if (fire) {
			background = new Background(new BackgroundFill(Color.BLACK, new CornerRadii(4), null));			
		}else{
			if (bResponsive) 
				background = new Background(new BackgroundFill(Color.BLUEVIOLET, null, null));
			//setBorder(null);
		}
		setBackground(background);
	}

	private void scaleChecker(){
		if(getScaleX() > fromx){
			setScaleX(fromx);
		}
		if(getScaleY() > fromy){
			setScaleY(fromy);
		}
	}

	@Override
	public void arm() {
		// TODO Auto-generated method stub
		super.arm();
	}

	@Override
	public void disarm() {
		// TODO Auto-generated method stub
		super.disarm();
	}


	@Override
	protected void updateBounds() {
		super.updateBounds();
	}

	public boolean isbResponsive() {
		return bResponsive;
	}

	public void setbResponsive(boolean bResponsive) {
		this.bResponsive = bResponsive;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

}
